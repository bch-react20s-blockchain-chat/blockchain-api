import User from "../model/Users.js";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import "../services/mongoDbClient.js";

// Registers new user
export const register = async (req, res) => {
  User.findOne({ userId: req.body.email }, (err, user) => {
    if (err) return res.status(500).send("Internal Server Error");
    if (user) return res.status(409).send("Email is in use");
  });
  let hashedPassword = bcrypt.hashSync(req.body.password, 8); //FIXME: add error handling
  const user = new User({
    userName: req.body.name,
    userId: req.body.email,
    password: hashedPassword,
  });

  const result = await user.save(); //FIXME: add error handling

  const { password, ...data } = result.toJSON();

  const token = jwt.sign({ _id: data._id }, "secret", {
    expiresIn: 86400,
  });

  res.cookie("jwt", token, { httpOnly: true, maxAge: 86400 });

  res.status(200).send({
    message: "success",
  });
};

// logs user in providing they provide correct credentials
export const login = (req, res) => {
  User.findOne({ userId: req.body.email }, (err, user) => {
    if (err) return res.status(500).send("Internal server error");
    if (!user) return res.status(401).send("user not found");

    const passwordIsValid = bcrypt.compareSync(
      req.body.password || "",
      user.password
    );
    if (!passwordIsValid) return res.status(401).send(user);

    const token = jwt.sign({ _id: user._id }, "secret", {
      expiresIn: 86400,
    });

    res.cookie("jwt", token, { httpOnly: true, maxAge: 86400 });

    res.status(200).send({
      message: "success",
      username: user.userName,
    });
  });
};

// used to retireve user information on the chatpage
export const retrieve = async (req, res) => {
  try {
    const cookie = req.cookies["jwt"];

    const claims = jwt.verify(cookie, "secret");

    // if cookie with name JWT does not exist return unauthenticated
    if (!claims) {
      return res.status(401).send({
        message: "unauthenticated",
      });
    }

    const user = await User.findOne({ _id: claims._id });

    // destructures the data object removing the password to ensure it isn't sent as response
    const { password, ...data } = user.toJSON();
    res.send(data);
  } catch (e) {
    return res.status(401).send({
      message: "unauthenticated",
    });
  }
};

//signs user out
export const signout = (req, res) => {
  res.cookie("jwt", { maxAge: 0 }); //FIXME this will expire the cookie client side (i.e. it won't use it anymore) but the token itself will still remain valid. We should probably also add this to some sort of db blacklist so the server can determine when to not accept a valid cookie.

  res.send("signed out");
};
