import lotionCli from "../services/lotionClient.js";
import * as mongoHandlers from "./mongoHandlers.js";

let currentChannelId = 1; // Maintain id of the channel the client is connected to
let currentChannel = `channel ${currentChannelId}`; // Helper to use as a socket.io event name

export const handleConnection = async (socket) => {
  console.log(
    `Client ${socket.id} connected, sending channels and message log of the default channel...`
  );

  socket.join(currentChannel);

  //loads channel message history, Array of messages
  const result = await mongoHandlers.loadOnLogIn(currentChannelId);

  //loads the channels list  fron DB
  const channelList = await mongoHandlers.loadChannels();

  // Try to read the blockchain state and emit back all channels and messages of current channel
  lotionCli
    .state()
    .then((state) => {
      socket.emit("channels", state.channels); //FIXME: emit also when new channels are added
      socket.emit(
        "messages",
        //reading message from db
        //FIXME:: reading message directly from blockchain
        result
        //reading message from blockchain
        //state.messages.filter((result) => result.channelId === currentChannelId)
      );

      console.log(`Connection established with ${socket.id}!`);
    })
    .catch(console.log);
};

export const handleDisconnect = (socket) => {
  console.log(`Client ${socket.id} disconnected`);
};

export const handleMessage = async (socket, msg) => {
  try {
    console.log(
      `Client ${socket.id} sent a message, trying to save to blockchain and broadcast...`
    );
    const status = await lotionCli.send(msg); //FIXME: error handling?
    const testStatus = status[Object.keys(status)[0]];

    if (!Object.keys(testStatus).includes("code")) {
      //saves new messages to message Collection
      mongoHandlers.updateMessages(msg);
    } else {
      console.log(testStatus);
    }

    socket.to(`channel ${msg.channelId}`).emit("message", msg); //FIXME: only if prev send success?

    console.log(`Message from ${socket.id} sent!`);
  } catch (error) {
    console.log(error);
  }
};

export const handleChannelChange = async (socket, channelId) => {
  console.log(
    `Client ${socket.id} leaving ${currentChannel} and joining channel ${channelId}...`
  );

  const newChannel = `channel ${channelId}`;

  socket.leave(currentChannel);
  socket.join(newChannel);
  currentChannelId = channelId;

  //loads channel specific messages as an Array of message object
  const result = await mongoHandlers.loadOnLogIn(channelId);

  // Read and emit back the messages of the other channel from the blockchain
  lotionCli.state().then((state) => {
    socket.emit(
      "messages",
      result
      //reads the messages from database

      //reading message from blockchain
      //state.messages.filter((msg) => msg.channelId === currentChannelId)
    );

    console.log(`Channel change with ${socket.id} successful!`);
  });
};

export const handleChannelAdd = (socket, newChannel) => {
  console.log(
    `Client ${socket.id} adding a new channel, trying to save to the blockchain...`
  );

  lotionCli.send(newChannel); //FIXME: error handling?

  lotionCli
    .state()
    .then((state) => {
      //adding new channel to database each time a channel is added
      mongoHandlers.updateChannel(state.channels[state.channels.length - 1]);

      socket.emit("channels", state.channels); //FIXME hacky, this could be handled client-side also?
      socket.broadcast.emit("channels", state.channels);

      console.log(`Channel add by ${socket.id} successful!`);
    })
    .catch(console.log);
};
