import Channel from "../model/Channel.js";
import Message from "../model/Messages.js";
import User from "../model/Users.js";
import mongoose from "../services/mongoDbClient.js"; //this imports initiates the connection to DB

export const clearCache = () => {
  // Drop previous state of the cache db.
  mongoose.connection.db.dropCollection(
    "messages",
    (error, result) =>
      error && console.log("Cannot drop MongoDB/messages:", error.codeName)
  );
  mongoose.connection.db.dropCollection(
    "channels",
    (error, result) =>
      error && console.log("Cannot drop MongoDB/channels:", error.codeName)
  );
  mongoose.connection.db.dropCollection(
    "users",
    (error, result) =>
      error && console.log("Cannot drop MongoDB/users:", error.codeName)
  );
};

export const buildCacheFromBlockchain = (state) => {
  if (state.channels)
    for (let channel of state.channels)
      new Channel(channel).save((error, result) => error && console.log(error));

  if (state.messages)
    for (let message of state.messages)
      new Message(message).save((error, result) => error && console.log(error));

  if (state.users)
    for (let user of state.users)
      new User(user).save((error, result) => error && console.log(error));
};

export const updateChannel = (channel) => {
  let NewChannel = new Channel(channel);
  NewChannel.save((err) => {
    if (err) return console.log("Cannot add to DB");
    console.log("Successfully added to DB");
  });
};

export const loadChannels = async () => {
  try {
    const result = await Channel.find().sort({ timestamp: -1 });
    return result;
  } catch (err) {
    console.log(err);
  }
};

//on login & channelchange loads the specific channel Messsage History
export const loadOnLogIn = async (channelId) => {
  try {
    const result = await Message.find({
      channelId: channelId,
    })
      .limit(15)
      .sort({ timestamp: -1 }); // limits history load to 15 and sorts by timestamp in descending order for latest message
    const test = result.sort(); //reverting order to dispaly latest first at bottom view
    return test;
  } catch (err) {
    console.log(err);
  }
};

export const updateMessages = (msg) => {
  let NewMessage = new Message(msg);
  NewMessage.save((err) => {
    if (err) return console.log("Cannot add to DB");
    console.log("Successfully added to DB");
  });
};

export const updateUsers = (user) => {
  let NewUser = new User(user);
  NewUser.save((err) => {
    if (err) return console.log("Cannot add to DB");
    console.log("Successfully added to DB");
  });
};
