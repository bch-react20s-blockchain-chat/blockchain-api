//holds Message Schema, stores them to the database
import mongoose from "mongoose";
const Schema = mongoose.Schema;

//create Schema for holding all users
let UsersSchema = new Schema({
  userId: {
    required: true,
    type: Schema.Types.String,
  },
  userName: {
    required: true,
    type: Schema.Types.String,
  },
  title: {
    type: Schema.Types.String,
  },
  avatar: {
    type: Buffer,
  },
  password: {
    type: Schema.Types.String,
  },
});

export default mongoose.model("User", UsersSchema);
