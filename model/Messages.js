//holds Message Schema, stores them to the database
import mongoose from "mongoose";
const Schema = mongoose.Schema;

//creates Schema for holding all
let MessageSchema = new Schema({
  channelId: {
    required: true,
    type: Schema.Types.Number,
  },
  userId: {
    required: true,
    type: Schema.Types.String,
  },
  content: {
    required: true,
    type: Schema.Types.String,
  },
  timestamp: {
    required: true,
    type: Schema.Types.Date,
  },
});

export default mongoose.model("Message", MessageSchema);
