//holds Channels Schema, stores them to the database
import mongoose from "mongoose";
const Schema = mongoose.Schema;

//create Schema for holding channels only
//all properties required, can not be empty
let ChannelSchema = new Schema({
  channelId: {
    required: true,
    type: Schema.Types.Number,
  },
  channelName: {
    required: true,
    type: Schema.Types.String,
  },
  channelDescription: {
    required: true,
    type: Schema.Types.String,
  },
});

export default mongoose.model("Channel", ChannelSchema);
