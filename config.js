import dotenv from "dotenv";
dotenv.config();

export const ENVIRONMENT = process.env.ENVIRONMENT;
export const MONGODB_CLIENT = {
  clearCache: process.env.MONGODB_CLEAR || false,
  uri: process.env.MONGODB_URI,
};

export const APP = {
  port: process.env.PORT || 4000,
  allowedOrigins: process.env.ALLOWED_ORIGINS
    ? process.env.ALLOWED_ORIGINS.split(",")
    : ["http://localhost:3000"],
};

export const LOTION_CLIENT = {
  gci: process.env.GCI,
  nodes: process.env.BLOCKCHAIN_NODES
    ? process.env.BLOCKCHAIN_NODES.split(",")
    : ["ws://localhost:26657"],
};
