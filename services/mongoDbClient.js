import mongoose from "mongoose";
import { MONGODB_CLIENT } from "../config.js";

export const initiateMongoConnection = async () =>
  new Promise((resolve, reject) => {
    mongoose.connect(
      MONGODB_CLIENT.uri,
      { useNewUrlParser: true, useUnifiedTopology: true },
      (error) =>
        error
          ? reject({ message: "Cannot connect to MongoDB:", error: error })
          : resolve({ message: "Connected to MongoDB!" })
    );
  });

export default mongoose;
