//connect will allow any computer (that can run NodeJS) to operate
import connect from "lotion-connect";
import genesis from "../genesis.js";
import { ENVIRONMENT, LOTION_CLIENT } from "../config.js";
import { PRODUCTION } from "../constants/constants.js";

class LotionClient {
  state = () => undefined;
  send = () => undefined;
  connected = false;

  initiateLotionConnection = () => {
    return new Promise(async (resolve, reject) => {
      try {
        console.log("Connecting with the blockchain...");
  
        let lotionConn;
  
        // For production connection, use genesis and nodes list, otherwise use a gci
        ENVIRONMENT === PRODUCTION
          ? (lotionConn = await connect(null, {
              genesis: genesis,
              nodes: LOTION_CLIENT.nodes,
            }))
          : (lotionConn = await connect(LOTION_CLIENT.gci));
  
        this.send = lotionConn.send;
        this.state = lotionConn.getState;
        this.connected = true;
  
        console.log("Connection with the blockchain established!");
        resolve();
      } catch (err) {
        console.log("Error connecting with the blockchain:", err);
        reject();
      }
    })
  };
}

export default new LotionClient();
