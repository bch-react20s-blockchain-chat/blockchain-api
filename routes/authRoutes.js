import express from "express";
import {
  register,
  login,
  retrieve,
  signout,
} from "../controllers/authController.js";

const router = express.Router();
router.post("/register", register);
router.post("/login", login);
router.get("/user", retrieve);
router.post("/signout", signout);

export default router;
