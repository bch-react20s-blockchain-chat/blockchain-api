import express from "express";
import { handleWeb } from "../controllers/webHandler.js";

const router = express.Router();
router.get("/", handleWeb);

export default router;
