import * as socketHandlers from "../controllers/socketHandlers.js";

const onConnection = (socket) => {
  socketHandlers.handleConnection(socket);

  socket.on("disconnect", () => socketHandlers.handleDisconnect(socket));

  socket.on("message", (msg) => socketHandlers.handleMessage(socket, msg));

  socket.on("channel change", (channelId) =>
    socketHandlers.handleChannelChange(socket, channelId)
  );

  socket.on("channel add", (newChannel) =>
    socketHandlers.handleChannelAdd(socket, newChannel)
  );
};

export default onConnection;
