export default {
  genesis_time: "2021-03-10T10:05:27.6030702Z",
  chain_id: "react20s-chat-blockchain",
  consensus_params: {
    block: {
      max_bytes: "22020096",
      max_gas: "-1",
      time_iota_ms: "1000",
    },
    evidence: {
      max_age: "100000",
    },
    validator: {
      pub_key_types: ["ed25519"],
    },
  },
  validators: [
    {
      address: "A37CB37FB54CB283A5BAEB703177E4117E882C61",
      pub_key: {
        type: "tendermint/PubKeyEd25519",
        value: "35fLeZegxJN2oAmbCOmyLfI8/2W2cFd9YOKE84YKrUo=",
      },
      power: "10",
      name: "",
    },
  ],
  app_hash: "",
};
