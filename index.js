import express from "express";
import cors from "cors";
import http from "http";
import { Server } from "socket.io";
import cookieParser from "cookie-parser";

import { APP, MONGODB_CLIENT } from "./config.js";
import routes from "./routes/webRoutes.js";
import authRoutes from "./routes/authRoutes.js";
import onConnection from "./routes/socketRoutes.js";
import lotionCli from "./services/lotionClient.js";
import { initiateMongoConnection } from "./services/mongoDbClient.js";
import {
  clearCache,
  buildCacheFromBlockchain,
} from "./controllers/mongoHandlers.js";

const app = express();
app.use(cookieParser());
app.use(express.json());

app.use(cors({ credentials: true, origin: true })); //FIXME: allowed origins should be read from config
app.use(authRoutes);
app.use(routes);

const server = http.createServer(app);

const io = new Server(server, {
  cors: {
    origins: APP.allowedOrigins,
  },
});

io.on("connection", onConnection);

lotionCli
  .initiateLotionConnection()
  .then(() => {
    initiateMongoConnection().then(async ({ message }) => {
      console.log(message);
      if (MONGODB_CLIENT.clearCache) {
        clearCache();
        buildCacheFromBlockchain(await lotionCli.state());
      }
    });

    const port = APP.port;
    server.listen(port, () => console.log(`Listening on port ${port}`));
  })
  .catch(console.log);
