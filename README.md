# Blockchain Api

![Demo](./demo.gif)

# npm start
Starts server

# npm run dev
starts the node in development mode with debugging enabled


# Starting your front end App 
- While starting your fron end app use "http://localhost:4000" as your end point
- When socket connection is established, 'New client connected' is printed in server console
- when connection ends or gets disrupted, 'Client disconnected' is loged in server console.

# VS code debugging configuration
- launch configuration for Node.js debugging

```json
{
  
  "version": "0.2.0",
  "configurations": [
    {
      "type": "node",
      "request": "launch",
      "name": "Launch Program",
      "skipFiles": [
        "<node_internals>/**"
      ],
      "program": "${workspaceFolder}/index.js"
    }
  ]
}
```